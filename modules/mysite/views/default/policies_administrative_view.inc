<?php

$view = new view();
$view->name = 'policies_administrative_view';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Policies Administrative View';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Policies and Procedures Administrative View';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'role';
$handler->display->display_options['access']['role'] = array(
  6 => '6',
  7 => '7',
  3 => '3',
);
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['distinct'] = TRUE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['grouping'] = array(
  0 => array(
    'field' => 'type',
    'rendered' => 1,
    'rendered_strip' => 0,
  ),
);
$handler->display->display_options['style_options']['columns'] = array(
  'type' => 'type',
  'title' => 'title',
  'field_policy_type' => 'field_policy_type',
  'field_procedure_type' => 'field_policy_type',
  'field_revised_date' => 'field_revised_date',
  'field_adopted_date' => 'field_adopted_date',
  'field_rescinded_date' => 'field_rescinded_date',
  'field_status' => 'field_status',
  'field_procedure_status' => 'field_status',
  'field_private' => 'field_private',
  'status' => 'status',
  'edit_node' => 'edit_node',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'type' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_policy_type' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_procedure_type' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_revised_date' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_adopted_date' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_rescinded_date' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_status' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_procedure_status' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_private' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'status' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'edit_node' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['sticky'] = TRUE;
/* Field: Content: Type */
$handler->display->display_options['fields']['type']['id'] = 'type';
$handler->display->display_options['fields']['type']['table'] = 'node';
$handler->display->display_options['fields']['type']['field'] = 'type';
$handler->display->display_options['fields']['type']['label'] = '';
$handler->display->display_options['fields']['type']['exclude'] = TRUE;
$handler->display->display_options['fields']['type']['element_label_colon'] = FALSE;
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Policy Type */
$handler->display->display_options['fields']['field_policy_type']['id'] = 'field_policy_type';
$handler->display->display_options['fields']['field_policy_type']['table'] = 'field_data_field_policy_type';
$handler->display->display_options['fields']['field_policy_type']['field'] = 'field_policy_type';
$handler->display->display_options['fields']['field_policy_type']['ui_name'] = 'Policy Type';
$handler->display->display_options['fields']['field_policy_type']['label'] = 'Type';
$handler->display->display_options['fields']['field_policy_type']['type'] = 'taxonomy_term_reference_plain';
/* Field: Procedure Type */
$handler->display->display_options['fields']['field_procedure_type']['id'] = 'field_procedure_type';
$handler->display->display_options['fields']['field_procedure_type']['table'] = 'field_data_field_procedure_type';
$handler->display->display_options['fields']['field_procedure_type']['field'] = 'field_procedure_type';
$handler->display->display_options['fields']['field_procedure_type']['ui_name'] = 'Procedure Type';
$handler->display->display_options['fields']['field_procedure_type']['type'] = 'taxonomy_term_reference_plain';
/* Field: Content: Revised Date */
$handler->display->display_options['fields']['field_revised_date']['id'] = 'field_revised_date';
$handler->display->display_options['fields']['field_revised_date']['table'] = 'field_data_field_revised_date';
$handler->display->display_options['fields']['field_revised_date']['field'] = 'field_revised_date';
$handler->display->display_options['fields']['field_revised_date']['settings'] = array(
  'format_type' => 'custom',
  'custom_date_format' => 'n/j/Y',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
$handler->display->display_options['fields']['field_revised_date']['delta_offset'] = '0';
/* Field: Content: Adopted Date */
$handler->display->display_options['fields']['field_adopted_date']['id'] = 'field_adopted_date';
$handler->display->display_options['fields']['field_adopted_date']['table'] = 'field_data_field_adopted_date';
$handler->display->display_options['fields']['field_adopted_date']['field'] = 'field_adopted_date';
$handler->display->display_options['fields']['field_adopted_date']['settings'] = array(
  'format_type' => 'custom',
  'custom_date_format' => 'n/j/Y',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
/* Field: Content: Rescinded Date */
$handler->display->display_options['fields']['field_rescinded_date']['id'] = 'field_rescinded_date';
$handler->display->display_options['fields']['field_rescinded_date']['table'] = 'field_data_field_rescinded_date';
$handler->display->display_options['fields']['field_rescinded_date']['field'] = 'field_rescinded_date';
$handler->display->display_options['fields']['field_rescinded_date']['settings'] = array(
  'format_type' => 'custom',
  'custom_date_format' => 'n/j/Y',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
/* Field: Policy Status */
$handler->display->display_options['fields']['field_status']['id'] = 'field_status';
$handler->display->display_options['fields']['field_status']['table'] = 'field_data_field_status';
$handler->display->display_options['fields']['field_status']['field'] = 'field_status';
$handler->display->display_options['fields']['field_status']['ui_name'] = 'Policy Status';
/* Field: Procedure Status */
$handler->display->display_options['fields']['field_procedure_status']['id'] = 'field_procedure_status';
$handler->display->display_options['fields']['field_procedure_status']['table'] = 'field_data_field_procedure_status';
$handler->display->display_options['fields']['field_procedure_status']['field'] = 'field_procedure_status';
$handler->display->display_options['fields']['field_procedure_status']['ui_name'] = 'Procedure Status';
$handler->display->display_options['fields']['field_procedure_status']['label'] = 'Procedure Status';
/* Field: Content: Private */
$handler->display->display_options['fields']['field_private']['id'] = 'field_private';
$handler->display->display_options['fields']['field_private']['table'] = 'field_data_field_private';
$handler->display->display_options['fields']['field_private']['field'] = 'field_private';
/* Field: Content: Published status */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'node';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['label'] = 'Published';
$handler->display->display_options['fields']['status']['not'] = 0;
/* Field: Content: Link to edit content */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['label'] = 'Operations';
/* Sort criterion: Content: Type */
$handler->display->display_options['sorts']['type']['id'] = 'type';
$handler->display->display_options['sorts']['type']['table'] = 'node';
$handler->display->display_options['sorts']['type']['field'] = 'type';
/* Sort criterion: Content: Title */
$handler->display->display_options['sorts']['title']['id'] = 'title';
$handler->display->display_options['sorts']['title']['table'] = 'node';
$handler->display->display_options['sorts']['title']['field'] = 'title';
/* Sort criterion: Content: Nid */
$handler->display->display_options['sorts']['nid']['id'] = 'nid';
$handler->display->display_options['sorts']['nid']['table'] = 'node';
$handler->display->display_options['sorts']['nid']['field'] = 'nid';
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'policy' => 'policy',
  'procedure_guideline' => 'procedure_guideline',
);
$handler->display->display_options['filters']['type']['group'] = 1;
/* Filter criterion: Content: Status (field_status) */
$handler->display->display_options['filters']['field_status_value']['id'] = 'field_status_value';
$handler->display->display_options['filters']['field_status_value']['table'] = 'field_data_field_status';
$handler->display->display_options['filters']['field_status_value']['field'] = 'field_status_value';
$handler->display->display_options['filters']['field_status_value']['group'] = 1;
$handler->display->display_options['filters']['field_status_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_status_value']['expose']['operator_id'] = 'field_status_value_op';
$handler->display->display_options['filters']['field_status_value']['expose']['label'] = 'Policy Status';
$handler->display->display_options['filters']['field_status_value']['expose']['operator'] = 'field_status_value_op';
$handler->display->display_options['filters']['field_status_value']['expose']['identifier'] = 'field_status_value';
$handler->display->display_options['filters']['field_status_value']['expose']['remember'] = TRUE;
$handler->display->display_options['filters']['field_status_value']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  6 => 0,
  4 => 0,
  8 => 0,
  3 => 0,
  7 => 0,
  9 => 0,
);
/* Filter criterion: Content: Status (field_procedure_status) */
$handler->display->display_options['filters']['field_procedure_status_value']['id'] = 'field_procedure_status_value';
$handler->display->display_options['filters']['field_procedure_status_value']['table'] = 'field_data_field_procedure_status';
$handler->display->display_options['filters']['field_procedure_status_value']['field'] = 'field_procedure_status_value';
$handler->display->display_options['filters']['field_procedure_status_value']['group'] = 1;
$handler->display->display_options['filters']['field_procedure_status_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_procedure_status_value']['expose']['operator_id'] = 'field_procedure_status_value_op';
$handler->display->display_options['filters']['field_procedure_status_value']['expose']['label'] = 'Procedure Status';
$handler->display->display_options['filters']['field_procedure_status_value']['expose']['operator'] = 'field_procedure_status_value_op';
$handler->display->display_options['filters']['field_procedure_status_value']['expose']['identifier'] = 'field_procedure_status_value';
$handler->display->display_options['filters']['field_procedure_status_value']['expose']['remember'] = TRUE;
$handler->display->display_options['filters']['field_procedure_status_value']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  6 => 0,
  4 => 0,
  8 => 0,
  3 => 0,
  7 => 0,
  9 => 0,
);
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = '0';
$handler->display->display_options['filters']['status']['exposed'] = TRUE;
$handler->display->display_options['filters']['status']['expose']['operator_id'] = '';
$handler->display->display_options['filters']['status']['expose']['label'] = 'Published';
$handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
$handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';
$handler->display->display_options['filters']['status']['expose']['remember'] = TRUE;
$handler->display->display_options['filters']['status']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  6 => 0,
  4 => 0,
  8 => 0,
  3 => 0,
  7 => 0,
  9 => 0,
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'admin/content/policies';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Policies/Procedures';
$handler->display->display_options['menu']['weight'] = '10';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
