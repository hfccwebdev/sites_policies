<?php

$view = new view();
$view->name = 'hfc_policies';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'HFC Policies';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'HFC Policies, Procedures, and Bylaws';
$handler->display->display_options['css_class'] = 'highlight-header';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['distinct'] = TRUE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'none';
$handler->display->display_options['style_plugin'] = 'list';
$handler->display->display_options['style_options']['grouping'] = array(
  0 => array(
    'field' => 'field_policy_type',
    'rendered' => 1,
    'rendered_strip' => 0,
  ),
);
$handler->display->display_options['row_plugin'] = 'fields';
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Policy Type */
$handler->display->display_options['fields']['field_policy_type']['id'] = 'field_policy_type';
$handler->display->display_options['fields']['field_policy_type']['table'] = 'field_data_field_policy_type';
$handler->display->display_options['fields']['field_policy_type']['field'] = 'field_policy_type';
$handler->display->display_options['fields']['field_policy_type']['label'] = '';
$handler->display->display_options['fields']['field_policy_type']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_policy_type']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_policy_type']['alter']['text'] = '<span id="[field_policy_type-tid]">[field_policy_type]</span>';
$handler->display->display_options['fields']['field_policy_type']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_policy_type']['type'] = 'taxonomy_term_reference_plain';
/* Sort criterion: Content: Title */
$handler->display->display_options['sorts']['title']['id'] = 'title';
$handler->display->display_options['sorts']['title']['table'] = 'node';
$handler->display->display_options['sorts']['title']['field'] = 'title';
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'policy' => 'policy',
);
/* Filter criterion: Content: Status (field_status) */
$handler->display->display_options['filters']['field_status_value']['id'] = 'field_status_value';
$handler->display->display_options['filters']['field_status_value']['table'] = 'field_data_field_status';
$handler->display->display_options['filters']['field_status_value']['field'] = 'field_status_value';
$handler->display->display_options['filters']['field_status_value']['operator'] = 'not';
$handler->display->display_options['filters']['field_status_value']['value'] = array(
  'board_review' => 'board_review',
);
/* Filter criterion: Content: Rescinded Date (field_rescinded_date) */
$handler->display->display_options['filters']['field_rescinded_date_value']['id'] = 'field_rescinded_date_value';
$handler->display->display_options['filters']['field_rescinded_date_value']['table'] = 'field_data_field_rescinded_date';
$handler->display->display_options['filters']['field_rescinded_date_value']['field'] = 'field_rescinded_date_value';

/* Display: approved */
$handler = $view->new_display('page', 'approved', 'page');
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'policy' => 'policy',
);
/* Filter criterion: Content: Status (field_status) */
$handler->display->display_options['filters']['field_status_value']['id'] = 'field_status_value';
$handler->display->display_options['filters']['field_status_value']['table'] = 'field_data_field_status';
$handler->display->display_options['filters']['field_status_value']['field'] = 'field_status_value';
$handler->display->display_options['filters']['field_status_value']['value'] = array(
  'approved' => 'approved',
);
/* Filter criterion: Content: Rescinded Date (field_rescinded_date) */
$handler->display->display_options['filters']['field_rescinded_date_value']['id'] = 'field_rescinded_date_value';
$handler->display->display_options['filters']['field_rescinded_date_value']['table'] = 'field_data_field_rescinded_date';
$handler->display->display_options['filters']['field_rescinded_date_value']['field'] = 'field_rescinded_date_value';
$handler->display->display_options['filters']['field_rescinded_date_value']['operator'] = 'empty';
$handler->display->display_options['path'] = 'hfc-policies/current';
$handler->display->display_options['menu']['type'] = 'default tab';
$handler->display->display_options['menu']['title'] = 'Current Policies';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'main-menu';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
$handler->display->display_options['tab_options']['type'] = 'normal';
$handler->display->display_options['tab_options']['title'] = 'Policies';
$handler->display->display_options['tab_options']['weight'] = '0';
$handler->display->display_options['tab_options']['name'] = 'main-menu';

/* Display: board approval */
$handler = $view->new_display('page', 'board approval', 'page_1');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'HFC Policies Pending Board Approval';
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['content'] = 'These policies have already been reviewed by the subcommittee, and are no longer available for public comment.';
$handler->display->display_options['header']['area']['format'] = 'markdown';
$handler->display->display_options['defaults']['empty'] = FALSE;
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['label'] = 'No Policies Message';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = 'There are no policies awaiting Board of Trustees approval at this time.';
$handler->display->display_options['empty']['area']['format'] = 'markdown';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'policy' => 'policy',
);
/* Filter criterion: Content: Status (field_status) */
$handler->display->display_options['filters']['field_status_value']['id'] = 'field_status_value';
$handler->display->display_options['filters']['field_status_value']['table'] = 'field_data_field_status';
$handler->display->display_options['filters']['field_status_value']['field'] = 'field_status_value';
$handler->display->display_options['filters']['field_status_value']['value'] = array(
  'board_review' => 'board_review',
);
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = '1';
/* Filter criterion: Content: Rescinded Date (field_rescinded_date) */
$handler->display->display_options['filters']['field_rescinded_date_value']['id'] = 'field_rescinded_date_value';
$handler->display->display_options['filters']['field_rescinded_date_value']['table'] = 'field_data_field_rescinded_date';
$handler->display->display_options['filters']['field_rescinded_date_value']['field'] = 'field_rescinded_date_value';
$handler->display->display_options['filters']['field_rescinded_date_value']['operator'] = 'empty';
$handler->display->display_options['path'] = 'hfc-policies/board-review';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Policies Under Board Review';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'main-menu';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;

/* Display: draft */
$handler = $view->new_display('page', 'draft', 'page_2');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'HFC Draft Policies on Hold';
$handler->display->display_options['defaults']['access'] = FALSE;
$handler->display->display_options['access']['type'] = 'role';
$handler->display->display_options['access']['role'] = array(
  6 => '6',
  7 => '7',
  3 => '3',
);
$handler->display->display_options['defaults']['empty'] = FALSE;
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['label'] = 'No Results Message';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = 'There are no draft policies at this time.';
$handler->display->display_options['empty']['area']['format'] = 'markdown';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'policy' => 'policy',
);
/* Filter criterion: Content: Status (field_status) */
$handler->display->display_options['filters']['field_status_value']['id'] = 'field_status_value';
$handler->display->display_options['filters']['field_status_value']['table'] = 'field_data_field_status';
$handler->display->display_options['filters']['field_status_value']['field'] = 'field_status_value';
$handler->display->display_options['filters']['field_status_value']['value'] = array(
  'hold' => 'hold',
);
/* Filter criterion: Content: Rescinded Date (field_rescinded_date) */
$handler->display->display_options['filters']['field_rescinded_date_value']['id'] = 'field_rescinded_date_value';
$handler->display->display_options['filters']['field_rescinded_date_value']['table'] = 'field_data_field_rescinded_date';
$handler->display->display_options['filters']['field_rescinded_date_value']['field'] = 'field_rescinded_date_value';
$handler->display->display_options['filters']['field_rescinded_date_value']['operator'] = 'empty';
$handler->display->display_options['path'] = 'hfc-policies/draft';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Draft Policies';
$handler->display->display_options['menu']['weight'] = '20';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;

/* Display: subcommittee approval */
$handler = $view->new_display('page', 'subcommittee approval', 'page_4');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'HFC Policies Pending Subcommittee Approval';
$handler->display->display_options['defaults']['header'] = FALSE;
/* Header: Global: Text area */
$handler->display->display_options['header']['area']['id'] = 'area';
$handler->display->display_options['header']['area']['table'] = 'views';
$handler->display->display_options['header']['area']['field'] = 'area';
$handler->display->display_options['header']['area']['content'] = 'These policies are under review by the subcommittee, and are available for public comment for a limited time. If you have any changes for these policies please email them to Caryne Dematteo cadye@hfcc.edu.';
$handler->display->display_options['header']['area']['format'] = 'markdown';
$handler->display->display_options['defaults']['empty'] = FALSE;
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['label'] = 'No Policies Message';
$handler->display->display_options['empty']['area']['empty'] = TRUE;
$handler->display->display_options['empty']['area']['content'] = 'There are no policies awaiting subcommittee approval at this time.';
$handler->display->display_options['empty']['area']['format'] = 'markdown';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'policy' => 'policy',
);
/* Filter criterion: Content: Status (field_status) */
$handler->display->display_options['filters']['field_status_value']['id'] = 'field_status_value';
$handler->display->display_options['filters']['field_status_value']['table'] = 'field_data_field_status';
$handler->display->display_options['filters']['field_status_value']['field'] = 'field_status_value';
$handler->display->display_options['filters']['field_status_value']['value'] = array(
  'subcommittee_review' => 'subcommittee_review',
);
/* Filter criterion: Content: Published status */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = '1';
/* Filter criterion: Content: Rescinded Date (field_rescinded_date) */
$handler->display->display_options['filters']['field_rescinded_date_value']['id'] = 'field_rescinded_date_value';
$handler->display->display_options['filters']['field_rescinded_date_value']['table'] = 'field_data_field_rescinded_date';
$handler->display->display_options['filters']['field_rescinded_date_value']['field'] = 'field_rescinded_date_value';
$handler->display->display_options['filters']['field_rescinded_date_value']['operator'] = 'empty';
$handler->display->display_options['path'] = 'hfc-policies/subcommittee-review';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Policies Under Subcommittee Review ';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'main-menu';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;

/* Display: rescinded policies */
$handler = $view->new_display('page', 'rescinded policies', 'page_3');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Rescinded HFC Policies';
$handler->display->display_options['defaults']['access'] = FALSE;
$handler->display->display_options['access']['type'] = 'role';
$handler->display->display_options['access']['role'] = array(
  6 => '6',
  7 => '7',
  3 => '3',
);
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'field_policy_id' => 'field_policy_id',
  'title' => 'title',
  'field_policy_type' => 'field_policy_type',
  'field_status' => 'field_status',
  'field_adopted_date' => 'field_adopted_date',
  'field_rescinded_date' => 'field_rescinded_date',
  'field_rescinded_explanation' => 'field_rescinded_explanation',
  'status' => 'status',
);
$handler->display->display_options['style_options']['class'] = '';
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'field_policy_id' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_policy_type' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_status' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_adopted_date' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_rescinded_date' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_rescinded_explanation' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'status' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Field: Policy ID */
$handler->display->display_options['fields']['field_policy_id']['id'] = 'field_policy_id';
$handler->display->display_options['fields']['field_policy_id']['table'] = 'field_data_field_policy_id';
$handler->display->display_options['fields']['field_policy_id']['field'] = 'field_policy_id';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = 'Policy Name';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Policy Type */
$handler->display->display_options['fields']['field_policy_type']['id'] = 'field_policy_type';
$handler->display->display_options['fields']['field_policy_type']['table'] = 'field_data_field_policy_type';
$handler->display->display_options['fields']['field_policy_type']['field'] = 'field_policy_type';
$handler->display->display_options['fields']['field_policy_type']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_policy_type']['type'] = 'taxonomy_term_reference_plain';
/* Field: Content: Status */
$handler->display->display_options['fields']['field_status']['id'] = 'field_status';
$handler->display->display_options['fields']['field_status']['table'] = 'field_data_field_status';
$handler->display->display_options['fields']['field_status']['field'] = 'field_status';
/* Field: Content: Adopted Date */
$handler->display->display_options['fields']['field_adopted_date']['id'] = 'field_adopted_date';
$handler->display->display_options['fields']['field_adopted_date']['table'] = 'field_data_field_adopted_date';
$handler->display->display_options['fields']['field_adopted_date']['field'] = 'field_adopted_date';
$handler->display->display_options['fields']['field_adopted_date']['settings'] = array(
  'format_type' => 'short',
  'custom_date_format' => '',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
/* Field: Content: Rescinded Date */
$handler->display->display_options['fields']['field_rescinded_date']['id'] = 'field_rescinded_date';
$handler->display->display_options['fields']['field_rescinded_date']['table'] = 'field_data_field_rescinded_date';
$handler->display->display_options['fields']['field_rescinded_date']['field'] = 'field_rescinded_date';
$handler->display->display_options['fields']['field_rescinded_date']['settings'] = array(
  'format_type' => 'short',
  'custom_date_format' => '',
  'fromto' => 'both',
  'multiple_number' => '',
  'multiple_from' => '',
  'multiple_to' => '',
  'show_remaining_days' => 0,
);
/* Field: Content: Rescinded Explanation */
$handler->display->display_options['fields']['field_rescinded_explanation']['id'] = 'field_rescinded_explanation';
$handler->display->display_options['fields']['field_rescinded_explanation']['table'] = 'field_data_field_rescinded_explanation';
$handler->display->display_options['fields']['field_rescinded_explanation']['field'] = 'field_rescinded_explanation';
/* Field: Content: Published status */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'node';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['not'] = 0;
$handler->display->display_options['defaults']['sorts'] = FALSE;
/* Sort criterion: Content: Published status */
$handler->display->display_options['sorts']['status']['id'] = 'status';
$handler->display->display_options['sorts']['status']['table'] = 'node';
$handler->display->display_options['sorts']['status']['field'] = 'status';
$handler->display->display_options['sorts']['status']['order'] = 'DESC';
/* Sort criterion: Content: Title */
$handler->display->display_options['sorts']['title']['id'] = 'title';
$handler->display->display_options['sorts']['title']['table'] = 'node';
$handler->display->display_options['sorts']['title']['field'] = 'title';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'policy' => 'policy',
);
/* Filter criterion: Content: Rescinded Date (field_rescinded_date) */
$handler->display->display_options['filters']['field_rescinded_date_value']['id'] = 'field_rescinded_date_value';
$handler->display->display_options['filters']['field_rescinded_date_value']['table'] = 'field_data_field_rescinded_date';
$handler->display->display_options['filters']['field_rescinded_date_value']['field'] = 'field_rescinded_date_value';
$handler->display->display_options['filters']['field_rescinded_date_value']['operator'] = 'not empty';
$handler->display->display_options['path'] = 'hfc-policies/rescinded-policies';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Rescinded Policies';
$handler->display->display_options['menu']['weight'] = '50';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
